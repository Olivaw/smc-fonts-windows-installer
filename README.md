 See *COPYING* for License terms

# PRE-REQUISITES

1. Visual Studio 2013 Community / Professional / Ultimate editions
2. Wix 3.9 RC2 or greater
3. Windows machine with Powershell (for pre build events)

# BUILD PROCESS

preBuild.ps1 is a powershell script that will download the font files for packaging. 
The rest is simple WiX directives, directly referenced from Wix documentation. Note that this requires that the user has Admin privileges on the machine.